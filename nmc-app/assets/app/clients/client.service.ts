import { Http, Response , Headers} from "@angular/http"
import {Injectable} from "@angular/core";
import 'rxjs/Rx'
import {Observable} from "rxjs"

@Injectable()
export  class ClientService {

    constructor(private http:Http) {}

    getClients() {
        return this.http.get('http://localhost:3000/clients')
            .map((response:Response) => {
             return response.json().obj;
            })
            .catch((error:Response) => Observable.throw(error.json()));
    }

}