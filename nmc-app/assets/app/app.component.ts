import { Component } from '@angular/core';
import {ClientService} from "./clients/client.service";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers : [ClientService]
})
export class AppComponent {

    constructor( protected  service: ClientService) {
        this.service.getClients().subscribe(
            (data) => (console.log(data))
        );
    }
}