var express = require('express');
var https = require('https');
var router = express.Router();

/* GET clients. */
router.get('/', function(req, res, next) {
    getClients(res)
});

function getClients(res) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    return https.get({
        host: '10.31.183.94',
        port: '9090',
        path: '/nwrestapi/v2/global/clients',
        headers : {
            'Content-Type':'application/json',
            'Accept':'application/json',
            'Authorization':'Basic YWRtaW5pc3RyYXRvcjpDaGFuZ2VtZUAxMjM='
        }
    }, function(response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
            // Data reception is done, do whatever with it!
            var parsed = JSON.parse(body);
            console.log(parsed);
            res.status(200).json({
                message: 'Success',
                obj:parsed
            });
        });
    });
}
module.exports = router;

